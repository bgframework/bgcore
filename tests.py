import unittest as ut

# Test label filter ---------------------------------------------------------------------------------------------------------

from bgcore.labelfilter import LabelFilter

class TestLabelFilter(ut.TestCase):

	def test_empty_filter(self):
		f = LabelFilter()
		for label in ["A", "B", "C"]:
			self.assertTrue(f.valid(label))

	def test_include(self):
		f = LabelFilter(include=set(["A", "B"]))
		self.assertTrue(f.valid("A"))
		self.assertTrue(f.valid("B"))
		self.assertFalse(f.valid("C"))

	def test_exclude(self):
		f = LabelFilter(exclude=set(["A", "B"]))
		self.assertFalse(f.valid("A"))
		self.assertFalse(f.valid("B"))
		self.assertTrue(f.valid("C"))

	def test_both(self):
		f = LabelFilter(include=set(["A", "B"]), exclude=set(["B", "C"]))
		self.assertTrue(f.valid("A"))
		self.assertFalse(f.valid("B"))
		self.assertFalse(f.valid("C"))
		self.assertFalse(f.valid("D"))

	def test_counts(self):
		f = LabelFilter(include=set(["A", "B"]), exclude=set(["B"]))
		self.assertEqual(f.include_count, 2)
		self.assertEqual(f.exclude_count, 1)

	def test_load_file(self):
		import os
		import tempfile
		path = tempfile.mkstemp()[1]
		with open(path, "w") as f:
			f.write("\n".join(["A", "B", "-B", "-C"]))
		f = LabelFilter()
		f.load(path)
		os.remove(path)
		self.assertTrue(f.valid("A"))
		self.assertFalse(f.valid("B"))
		self.assertFalse(f.valid("C"))
		self.assertFalse(f.valid("D"))

	def test_load_stream(self):
		from StringIO import StringIO
		f = LabelFilter()
		f.load(StringIO("\n".join(["A", "B", "-B", "-C"])))
		self.assertTrue(f.valid("A"))
		self.assertFalse(f.valid("B"))
		self.assertFalse(f.valid("C"))
		self.assertFalse(f.valid("D"))

# ReState -------------------------------------------------------------------------------------------------------------

import re
from bgcore.re import ReContext

class TestReContext(ut.TestCase):

	def test_match_string(self):
		m = ReContext()
		if m.match(r"^(.)B$", "B"):
			self.fail("Unexpected match")
		elif m.match(r"^(X|Y)Z", "xZ", re.IGNORECASE):
			self.assertEqual(len(m.groups()), 1)
			self.assertEqual(m.group(1), "x")
		else:
			self.fail("Unexpected not matching")

	def test_match_compiled(self):
		pat1 = re.compile(r"^(.)B$")
		pat2 = re.compile(r"^(X|Y)Z", re.IGNORECASE)
		m = ReContext()
		if m.match(pat1, "B"):
			self.fail("Unexpected match")
		elif m.match(pat2, "xZ"):
			self.assertEqual(len(m.groups()), 1)
			self.assertEqual(m.group(1), "x")
		else:
			self.fail("Unexpected not matching")

	def test_match_compiled_flags(self):
		pat = re.compile(r"^(X|Y)Z", re.IGNORECASE)
		m = ReContext()
		self.assertRaises(TypeError, m.match, pat, "xZ", re.IGNORECASE)
		self.assertIsNotNone(m.match(pat, "xZ"))
		self.assertEqual(m.group(1), "x")

	def test_match_with(self):
		with ReContext() as m:
			m.match("^(X|Y)Z", "xZ")

if __name__ == "__main__":
	ut.main()