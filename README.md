Biomedical Genomics Python core libraries
=========================================

This project includes common Python code used in many other projects of the group.

Please see LICENSE and LICENSE.txt for further information about the license.

tsv
---

Utilities to work with tab separated values files. It has support for comments and parameters on comments.

request
-------

It includes a class to make http requests that supports maximum number of retries and maximum number of requests per second.

logging
-------

Wrapper for the standard Python logging with some utility functions

re
--

ReContext which allows to perform regex operations and save the resulting state (MatchObject) for further manipulation.
Example:

	matcher = ReState()
	if matcher.match("^(.)B$", "B"):
		print matcher.group(1)
	elif matcher.match("^(X|Y)Z$", "XZ"):
		print matcher.group(1), matcher.start(1)

labelfilter
-----------

LabelFilter which allows to filter labels according to include and exclude lists.

obo
---

A simple OBO parser.