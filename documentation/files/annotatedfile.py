import pandas as pd
from bgcore.files import open_file

# Read a normal tsv file with pandas
data = pd.read_csv('data_without_annotations.txt', sep='\t')

# Save a copy without any commented header
data.to_csv(open_file('data_without_annotations_copy.txt', mode='w'),
            sep='\t', index=None)

# Save a copy as a gzip file
data.to_csv(open_file('data_without_annotations_copy.txt.gz', mode='w'),
            sep='\t', index=None)

# Define some comments
comments = [
    "First line of comments",
    "Second line of comments"
]

# Define some annotations
annotations = {
    "tag1": "value1",
    "tag2": "value2"
}

# Save dataset with annotations and comments
data.to_csv(open_file('data_with_annotations.txt', comments=comments, values=annotations),
            sep="\t", index=None
)

# Save compress dataset with annotations and comments
data.to_csv(open_file('data_with_annotations.txt.gz', comments=comments, values=annotations),
            sep="\t", index=None
)


# Read dataset with annotations and comments
with open_file('data_with_annotations.txt') as fd:
    data2 = pd.read_csv(fd, sep="\t")
    comments2 = fd.get_comments()
    values2 = fd.get_values()

    print("--------------------------------------------")
    print("data2 = {}", data2)
    print("comments2 = {}", comments2)
    print("values2 = {}", values2)

# Read a compress dataset with annotations and comments
with open_file('data_with_annotations.txt.gz') as fd:
    data2 = pd.read_csv(fd, sep="\t")
    comments2 = fd.get_comments()
    values2 = fd.get_values()

    print("--------------------------------------------")
    print("data2 = {}", data2)
    print("comments2 = {}", comments2)
    print("values2 = {}", values2)

