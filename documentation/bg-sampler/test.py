import timeit

setup = '''import gzip
import pandas as pd
import numpy as np
from array import array

def v1():
    na = pd.read_csv(gzip.open('test.tsv.gz', 'r'), header=None)
    assert len(na) == 10000

def v2():
    na = pd.read_hdf('test.hdf', key='test')
    assert len(na) == 10000

def v3():
    na = pd.read_csv(open('test.tsv', 'r'), header=None)
    assert len(na) == 10000

def v4():
    with open('test.bin', 'rb') as f:
        na = array('d')
        na.fromstring(f.read())
        assert len(na) == 10000

def v5():
    with open('test.bin', 'rb') as f:
        na = array('d')
        na.fromfile(f, 10000)
        assert len(na) == 10000

def v6():
    na = np.fromfile('test.bin', dtype=float)
    assert len(na) == 10000
'''

repetitions = 1000
print(timeit.timeit('v1()', setup=setup, number=repetitions))
print(timeit.timeit('v2()', setup=setup, number=repetitions))
print(timeit.timeit('v3()', setup=setup, number=repetitions))
print(timeit.timeit('v4()', setup=setup, number=repetitions))
print(timeit.timeit('v5()', setup=setup, number=repetitions))
print(timeit.timeit('v6()', setup=setup, number=repetitions))