import gzip
import sys
from array import array

in_file = sys.argv[1]
out_file = sys.argv[2]

with gzip.open(in_file, 'r') as in_f, open(out_file, 'w') as out_f:
    na = array('d', [float(v) for v in in_f])
    na.tofile(out_f)
