from bgcore.variants.parser.factory import VariantParserFactory


filename = "../files/somemuts.maf"

# Create a VariantParser of correct type with the factory
variant_parser = VariantParserFactory.get("maf", open(filename), filename, "example")

# iterate the variants in the file
for variant in variant_parser:

    # compute the variant end position (compatible with the VEP specifications)
    variant.compute_end()
    print("\t".join([str(x) for x in [variant.chr, variant.start, variant.end, variant.ref, variant.alt, variant.samples[0].name, variant.type]]))
