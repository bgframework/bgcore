# Block gzip constants
BLOCK_HEADER_LENGTH = 18
BLOCK_LENGTH_OFFSET = 16
BLOCK_FOOTER_LENGTH = 8

# Virtual file pointer constants
SHIFT_AMOUNT = 16
OFFSET_MASK = 0xffff
ADDRESS_MASK = 0xFFFFFFFFFFFF

# Compression level
COMPRESSION_LEVEL = 5

# Gzip overhead is the header, the footer, and the block size (encoded as a short).
GZIP_OVERHEAD = BLOCK_HEADER_LENGTH + BLOCK_FOOTER_LENGTH + 2

# If Deflater has compression level == NO_COMPRESSION, 10 bytes of overhead (determined experimentally).
NO_COMPRESSION_OVERHEAD = 10

# Push out a gzip block when this many uncompressed bytes have been accumulated.
# This size is selected so that if data is not compressible, if Deflater is given
# compression level == NO_COMPRESSION, compressed size is guaranteed to be <= MAX_COMPRESSED_BLOCK_SIZE.
DEFAULT_UNCOMPRESSED_BLOCK_SIZE = 64 * 1024 - (GZIP_OVERHEAD + NO_COMPRESSION_OVERHEAD)


