from itertools import chain, islice
import gzip
import time


def chunks(iterable, n):
    iterable = iter(iterable)
    while True:
        # store one line in memory,
        # chain it to an iterator on the rest of the chunk
        yield chain([next(iterable)], islice(iterable, n - 1))


def splitAndSort(filename, chunk_size=10 ** 6, compress_level=5):
    with open(filename, "rb") as file:
        for i, lines in enumerate(chunks(file, chunk_size)):
            file_split = '{}.{}'.format(filename, i)
            with gzip.open(file_split, 'wb', compresslevel=compress_level) as f:
                s_lines = list(lines)
                s_lines.sort()
                f.writelines(s_lines)
    return i + 1


def sort(filename, chunk_size=10 ** 6, compress_level=5):
    chunks = splitAndSort(filename, chunk_size=chunk_size, compress_level=compress_level)

    # TODO merge
    #heapq.merge()
    #for i in range(0, chunks):
    #sort_one_file('{}.{}'.format(filename, i))


start_time = time.time()
sort('test.tdm', chunk_size=10 ** 6, compress_level=5)
elapsed_time = time.time() - start_time

print(elapsed_time)




