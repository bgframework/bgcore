import zlib
import struct

from bgcore.mtabix.constants import *

__gzip_header = b"\x1f\x8b\x08\x04\x00\x00\x00\x00\x00\xff\x06\x00\x42\x43\x02\x00"


def compress(infile, outfile=None):
    if outfile is None:
        outfile = infile + ".gz"

    input = open(infile, "rb")
    output = open(outfile, "wb")

    i = 0
    while True:

        # Read a block
        block = input.read(DEFAULT_UNCOMPRESSED_BLOCK_SIZE)

        # Compress and write a block
        __write_block(output, block)

        if len(block) != DEFAULT_UNCOMPRESSED_BLOCK_SIZE:
            break

        i += 1

    output.close()
    input.close()


def __write_block(output, block):
    c = zlib.compressobj(COMPRESSION_LEVEL,
                         zlib.DEFLATED,
                         -15,
                         zlib.DEF_MEM_LEVEL,
                         0)
    compressed = c.compress(block) + c.flush()
    del c

    crc = zlib.crc32(block)

    # Should cope with a mix of Python platforms...
    if crc < 0:
        crc = struct.pack("<i", crc)
    else:
        crc = struct.pack("<I", crc)

    bsize = struct.pack("<H", len(compressed) + 25)  # includes -1
    crc = struct.pack("<I", zlib.crc32(block) & 0xffffffff)
    uncompressed_length = struct.pack("<I", len(block))

    #Fixed 16 bytes,
    # gzip magic bytes (4) mod time (4),
    # gzip flag (1), os (1), extra length which is six (2),
    # sub field which is BC (2), sub field length of two (2),
    #Variable data,
    #2 bytes: block length as BC sub field (2)
    #X bytes: the data
    #8 bytes: crc (4), uncompressed data length (4)
    data = __gzip_header + bsize + compressed + crc + uncompressed_length
    output.write(data)















