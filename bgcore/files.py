import gzip
import io
import datetime


def open_file(file_name, mode=None, values=None, comments=None, commentchar="#", writedate=False, encoding=None, errors=None, newline=None, compresslevel=9):
    """
        It opens a file descriptor. If the file extension ends with '.gz' it will
        use the 'gzip.open' function. Otherwise it will use the builtin 'open'.

        If you pass values and/or comments it will write them as a commented header.
        If you are reading a file with a commented header it will parse them

    :param file_name: File path
    :param mode: Open mode
    :return: A file descriptor
    """

    # If we have values or comments we open the file on write mode,
    # otherwise as read mode
    if mode is None:
        mode = "r" if values is None and comments is None else "w"

    if file_name.endswith(".gz"):
        gz_mode = mode.replace("t", "")
        fd = gzip.GzipFile(file_name, gz_mode, compresslevel)
    else:
        fd = io.FileIO(file_name, mode)

    return AnnotatedFile(fd,
                         values=values,
                         comments=comments,
                         commentchar=commentchar,
                         writedate=writedate,
                         encoding=encoding,
                         errors=errors,
                         newline=newline)


def count_lines(file_name):
    """

        Counts how many lines has a the given file

    :param file_name: A file path
    :return: Number of lines of the file (0 for empty files)
    """
    lines = 0
    with open_file(file_name, 'rb') as f:
        for line in f:
            lines += 1

    return lines


class AnnotatedFile(io.TextIOWrapper):
    def __init__(self, fd, values=None, comments=None, commentchar="#", writedate=False,
                 encoding=None, errors=None, newline=None):
        super().__init__(fd, encoding, errors, newline)

        self.values = values
        self.comments = comments

        self._commentchar = commentchar

        if writedate:
            if comments is None:
                comments = []

            comments.append(str(datetime.datetime.today()))

        self.writedHeader = False

    def read(self, *args, **kwargs):
        line = super().readline()
        while line.startswith(self._commentchar):
            if line.startswith(self._commentchar + self._commentchar):
                if self.values is None:
                    self.values = {}
                k, v = line.replace(self._commentchar + self._commentchar + " ", "").split("=")
                self.values[k.strip()] = v.strip()
            else:
                if self.comments is None:
                    self.comments = []
                self.comments.append(line.replace(self._commentchar, "").strip())
            line = super().readline()

        return line

    def write(self, *args, **kwargs):

        # First write the header comments
        if not self.writedHeader:

            if self.comments is not None:
                super().write("\n".join([self._commentchar + " " + comment for comment in self.comments]))
                super().write("\n")

            if self.values is not None:
                super().write("\n".join([self._commentchar + self._commentchar + " " + str(k) + "=" + str(v) for k, v in self.values.items()]))
                super().write("\n")

            self.writedHeader = True

        super().write(*args, **kwargs)

    def __iter__(self):
        return self

    def get_comments(self):
        return self.comments

    def get_values(self):
        return self.values

