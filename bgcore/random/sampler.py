import argparse
import datetime
import json
import logging
import os
import random
from statistics import mean
import math

from ago import human
import scipy.stats
import numpy as np

from bgcore.multiprocess.qmap import QMapExecutor
from bgcore.files import count_lines, open_file


def run(input_file, output_dir, num_lines, num_column=0, num_samples=1, num_randomizations=10000, separator='\t'):

    basename = os.path.splitext(os.path.splitext(os.path.basename(input_file))[0])[0]
    filename = "{0}-{1}-{2}-{3}.tsv.gz".format(basename, num_column, num_randomizations, num_samples)
    fitname = "{0}-{1}-{2}-{3}.fit".format(basename, num_column, num_randomizations, num_samples)
    output_file = os.path.join(output_dir, filename)
    fit_file = os.path.join(output_dir, fitname)

    logging.info("Calculating {0} groups of {1} random lines".format(num_randomizations, num_samples))
    start = datetime.datetime.now()
    random_sampling = list()
    random_set = set()

    # Use the number of lines, column number and number of samples as a random seed
    random_seed = num_lines + num_samples + num_randomizations
    random.seed(random_seed)

    for r in range(num_randomizations):
        samples = set()
        while len(samples) < num_samples:
            v = random.randint(0, num_lines - 1)
            if v not in samples:
                samples.add(v)
                random_set.add(v)
        random_sampling.append(samples)

    end = datetime.datetime.now()
    logging.info("Random values calculated {0}".format(human(start - end)))

    logging.info("Reading %d lines", len(random_set))
    start = datetime.datetime.now()
    values = {}
    with open_file(input_file, 'rb') as in_fd:
        for i, line in enumerate(in_fd):
            if i in random_set:
                values[i] = float(line.decode().split(separator)[num_column])
    end = datetime.datetime.now()
    logging.info("Lines readed %s", human(start - end))

    logging.info("Writing mean values to output file")
    start = datetime.datetime.now()
    random_means = np.empty([num_randomizations], dtype=np.float32)
    with open_file(output_file, 'wb') as out_fd:
        for i, sample in enumerate(random_sampling):
            sample_mean = mean([values[l] for l in sample])
            random_means[i] = sample_mean
            out_fd.write(bytes("{0}\n".format(sample_mean), 'UTF-8'))
    end = datetime.datetime.now()
    logging.info("Values writed to '%s' file %s", output_file, human(start - end))

    fitting = {}
    logging.info("Calculating Gamma parameters")
    fitting["Gamma"] = calculate_gamma_parameters(random_means)

    logging.info("Calculating zScore parameters")
    fitting["zScore"] = calculate_zscore_parameters(random_means)

    with open_file(fit_file, 'w') as out_fd:
        json.dump(fitting, out_fd)


def cmdline():
    # Config logging
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', datefmt='%H:%M:%S', level=logging.INFO)

    # Parse arguments
    parser = argparse.ArgumentParser()

    # Single process arguments
    parser.add_argument('-i', dest='input_file', help='Tabulated file to get random values', required=True)
    parser.add_argument('-o', dest='output_dir', default=None, help='Output folder (by default uses output folder)')
    parser.add_argument('--num_column', dest='num_column', default=0, type=int)
    parser.add_argument('--num_samples', dest='num_samples', default="1")
    parser.add_argument('--num_randomizations', dest='num_randomizations', default=100000, type=int)
    parser.add_argument('--num_lines', dest='num_lines', default=None, type=int)
    parser.add_argument('--from_samples', dest='from_samples', default=0, type=int)
    parser.add_argument('--to_samples', dest='to_samples', default=0, type=int)

    # Cluster options
    parser.add_argument('-j', '--max_jobs', dest='max_jobs', default=0, type=int,
                        help='Maximum running jobs')
    parser.add_argument('-q', '--queue', dest='queue', action='append', default=['normal'],
                        help='Cluster queues')
    parser.add_argument('--millions_per_core', dest='millions_per_core', default=18, type=int)
    parser.add_argument('--max_cores', dest='max_cores', default=0, type=int)
    parser.add_argument('--force_local', dest='force_local', default=False, action="store_true",
                        help='If you are in a cluster environment this will force the qmap to run only in '
                             'current machine')

    # Parse the arguments
    options = parser.parse_args()

    # Check that the input file exists
    input_file = options.input_file
    if not os.path.exists(input_file):
        raise RuntimeError("Input file {0} not found".format(input_file))

    # Use working directory if not specified
    output_dir = options.output_dir
    if output_dir is None:
        output_dir = os.path.join(os.getcwd(), 'output')
        if not os.path.exists(output_dir):
            os.mkdir(output_dir)

    # Logs directory
    logs_dir = os.path.join(output_dir, 'logs')
    if not os.path.exists(logs_dir):
        os.mkdir(logs_dir)

    # Count input file total number of lines if we don't known
    num_lines = options.num_lines
    if num_lines is None:
        logging.info("Calculating total number of lines")
        start = datetime.datetime.now()
        num_lines = count_lines(options.input_file)
        end = datetime.datetime.now()
        logging.info("The input file has %d lines. (calculated %s)", num_lines, human(start - end))

    # Run it
    if options.from_samples == 0 and options.to_samples == 0:

        num_samples = options.num_samples.split(",")

        # Single process run
        for num_sample in num_samples:
            run(
                input_file,
                output_dir,
                num_lines,
                num_column=options.num_column,
                num_samples=int(num_sample),
                num_randomizations=options.num_randomizations
            )
    else:

        # Build the arguments

        arguments = []
        cores_map = {}
        max_cores = options.max_cores if options.max_cores != 0 else os.cpu_count()

        f = options.from_samples
        t = options.to_samples
        s = int((t - f) / options.max_jobs)
        batches = [list(range(x, min(x+s, t+1))) for x in range(f, t+1, s)]

        for b in batches:

            # Estimate cores to use
            cores = int(max(1, min(
                max_cores,
                math.ceil((max(b) * options.num_randomizations) / (options.millions_per_core * 1000000))
            )))

            cmd = ['-i', options.input_file]
            cmd += ['-o', output_dir]
            cmd += ['--num_column', str(options.num_column)]
            cmd += ['--num_randomizations', str(options.num_randomizations)]
            cmd += ['--num_lines', str(num_lines)]
            cmd += ['--num_samples', ','.join([str(s) for s in b])]

            command = " ".join(cmd)
            cores_map[command] = cores
            arguments.append(command)

        # Multiprocess run
        QMapExecutor(options.queue, options.max_jobs, cores_map, force_local=options.force_local).run(
            'bg-sampler',
            arguments,
            job_name='bg-sampler',
            output_folder=logs_dir
        )


def calculate_gamma_parameters(values):
    """ Calculate the parameters of a gamma distribution
    :param values:
    """
    parameter_names = ("Shape", "Location", "Scale")
    parameters = {k: float(v) for k, v in zip(parameter_names, scipy.stats.gamma.fit(values))}
    return parameters


def calculate_zscore_parameters(values):
    """ Calculate the mu and std of a sample distribution
    :param values:
    """
    parameters = {"Mean": float(np.mean(values)), "Std": float(np.std(values))}
    return parameters

if __name__ == "__main__":
    cmdline()